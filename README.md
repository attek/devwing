FELADAT
-------------------
Egy alapszintű, egyszerű hírfolyam megvalósítása a feladat. A főoldalon egy hírlista jelenik meg, a
hírek címével, rövid leírásával, a hír dátumával, thumbnail képpel és egy tovább gombbal. A tovább
gombra kattintva megnyílik a hír oldala, melyen megjelenik a hír címe, a hír dátuma, a hír képe,
hosszabb leírása és egy vissza gomb.

TECHNOLOGIA
------------
Adatbázis, Git használat bármilyen Laravel technológiával, framework-kel megoldható.


MEGVALÓSÍTÁS
------------
Feladatot Yii2 keretrendszer segítségével oldottam meg. MVC design pattern-el.
Két model vany az egyik a User ami felhasználókat írja le és a News ami a híreket.
Controllekben van megvalósítva CRUD a felhasználók esetén a UserController, hír esetén pedig a NewsControllerben.
SiteController a bejelentkezésért, kijelentkezésért és statikus About oldalért felel.
Alapszintű hozzáférés korlátozás van ami a Yii2 beépített AccessControl-ja lát el. Ezzel van a Controllerek 
egyes metódusainak hozzáférés szabályozva.

Gyors megvalósítás érdekében fálj feltöltő és az képkezelő modul third party Yii2 modullal került megoldásra.

MŰKÖDÉS
-------
/site/login oldalon a megadott felhasználó névvel és jelszóval bejelentkezve lehet szerkeszteni a felhasználókat
/user és a híreket a /news oldalakon.
Főoldalon a hírek listája található ami az egyes híreknél a tovább gombra a hír egyedi oldalára vissz.
Az egyedi hír oldalról a vissza gombbal lehet a főoldalra jutni.

INSTALL
-------
git clone git@bitbucket.org:attek/devwing.git után
sh install.sh futtatása után az oldal a localhost:8000 -en lesz elérhető.
Az oldal linux oprenceren lett tesztelve.