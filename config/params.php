<?php

return [
    'upload_dir' => dirname(__DIR__) . DIRECTORY_SEPARATOR .  'web' . DIRECTORY_SEPARATOR . 'upload',
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
];
