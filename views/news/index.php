<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hírek';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Hír létrehozása', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'slug',
            'cr_date',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getStatus();
                },
            ],

          [
            'header' =>  'Műveletek',
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
              'view' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>','#', [
                  'title' => 'Megtekintés',
                  'class' => 'activity-view-link',
                  'data-id' => $key,
                  'data-url' => $url,
                  'data-title' => 'Felhasználó megtekintése',
                  'data-pjax' => '0',
                ]);
              },
              'delete' => function ($url, $model, $key) {
                return $model->status !== $model::STATUS_DELETED ?
                  Html::a('<span class="glyphicon glyphicon-trash"></span>','#', [
                    'title' => 'Törlés',
                    'class' => 'activity-delete-link',
                    'data-id' => $key,
                    'data-url' => $url,
                    'data-text' => Yii::t('app', 'törlöd {title} hírt?',
                      ['title' => $model->title]),
                    'data-pjax' => '0',
                  ]) : '';
              },
            ],
          ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?php
echo $this->render('@app/views/layouts/_modals');