<?php

use app\models\base\ActiveRecordStatus;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
      'options' => [
              'enctype' => 'multipart/form-data',
      ]
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'teaser')->textarea(['rows' => 4]) ?>

    <?= $form->field($model, 'body')->widget(CKEditor::class, [
      'options' => ['rows' => 6],
      'preset' => 'basic'
    ]); ?>

    <?php
    try {
        echo FileInput::widget( [
          'model'         => $model,
          'attribute'     => 'file',
          'pluginLoading' => true,
          'options'       => [
            'multiple'             => false,
            'id'                   => 'file-input',
            'data-msg-placeholder' => 'Fájl kiválasztása'
          ],
          'pluginOptions' => [
            'allowedFileExtensions' => [ 'png', 'jpg', 'jpeg' ],
            'initialPreview' => !$model->isNewRecord ? '/upload/' . $model->file_name : '',
            'initialPreviewConfig' => !$model->isNewRecord ? [
              'caption' => $model->title,
              'key' => $model->id,
              'url' => '/news/attachment-delete'
            ] : '',
            'initialPreviewAsData'  => true,
            'showUpload'            => false,
            'maxFileSize'           => 2048,
            'showPreview'           => true,
            'showRemove'            => true,
            'fileActionSettings'    => [
              'showRemove' => true,
              'showDrag'   => false,
            ],
          ],

        ] );
    } catch ( \Exception $e ) {
        Yii::error( $e->getMessage() );
        echo "Nem várt hiba történt, kérlek probáld meg később.";
    }
    ?>


    <?= $form->field($model, 'status')->dropDownList(ActiveRecordStatus::statusLabels()) ?>

    <div class="form-group">
      <?= Html::submitButton($model->isNewRecord
        ? 'Mentés' : 'Módosítás',
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
