<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */
?>

<div class="well">

	<?php try {
	    echo DetailView::widget([
			'model' => $model,
			'attributes' => [
				'id',
				'title',
				'slug',
				'teaser',
				[
					'attribute' => 'cr_user',
					'value' => $model->crUser !== null ? $model->crUser->name : null,
				],
				'cr_date',
				[
					'attribute' => 'mod_user',
					'value' => $model->modUser !== null ? $model->modUser->name : null,
					'visible' => $model->modUser !== null
				],
				[
					'attribute' => 'mod_date',
					'value' => $model->mod_date,
					'visible' => $model->mod_date !== null
				],
				[
					'attribute' => 'status',
					'value' => $model->getStatus(),
				],
			],
		]);
    } catch (\Exception $e ) {
		echo "Sajnos hiba történt, kérlek próbáld meg késöbb újra! " . $e->getMessage();
    }
    ?>

</div>
