<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hírek';
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_news',
    ]);
    ?>

    <?php Pjax::end(); ?>

</div>