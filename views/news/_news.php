<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $model app\models\News */
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="news-teaser">
                <img src="/upload/thumbnail/<?= $model->file_name?>" >
                <div>
                    <h3><?= Html::encode($model->title) ?></h3>
                    <?= Html::encode($model->teaser) ?>
                    <div><?= $model->cr_date ?></div>
                </div>
            </div>
        </div>
        <a href="/hir/<?= Html::encode($model->slug) ?>" title="<?= Html::encode($model->title) ?>" class="btn btn-primary next">Tovább</a>
    </div>


