<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\models\News */

?>
<div class="news">

    <h1><?= Html::encode($model->title) ?></h1>
    <div><img src="/upload/<?= $model->file_name ?>" title="<?= Html::encode($model->title) ?>" class="img-responsive"></div>
    <div><?= Html::encode($model->teaser) ?></div>
    <div><?= $model->body ?></div>
    <?= Html::a('Vissza', ['/'], ['class'=>'btn btn-primary']) ?>

</div>