<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Felhasználók';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Felhasználó létrehozása', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'name',
            'email:email',
            'phone',
            [
              'attribute' => 'status',
              'value' => function($model) {
                return $model->getStatus();
              },
            ],
          [
            'header' =>  'Műveletek',
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
              'view' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>','#', [
                  'title' => 'Megtekintés',
                  'class' => 'activity-view-link',
                  'data-id' => $key,
                  'data-url' => $url,
                  'data-title' => 'Felhasználó megtekintése',
                  'data-pjax' => '0',
                ]);
              },
              'delete' => function ($url, $model, $key) {
                return $model->status !== $model::STATUS_DELETED ?
                  Html::a('<span class="glyphicon glyphicon-trash"></span>','#', [
                    'title' => 'Törlés',
                    'class' => 'activity-delete-link',
                    'data-id' => $key,
                    'data-url' => $url,
                    'data-text' => Yii::t('app', 'törlöd {name} felhasználót?',
                      ['name' => $model->name]),
                    'data-pjax' => '0',
                  ]) : '';
              },
            ],
          ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?php
echo $this->render('@app/views/layouts/_modals');
