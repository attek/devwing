<?php
use \yii\bootstrap\Modal;
?>
<?php Modal::begin([
    'id' => 'activity-modal',
    'header' => '<h4 class="modal-title"></h4>',
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Bezár</a>',

]); ?>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'delete-modal',
    'header' => '<h4 class="modal-title">Biztos?</h4>',
    'footer' => '<button type="button" class="btn btn-primary" data-dismiss="modal" id="delete-confirm">Igen</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id="delete-cancel">Nem</button>',

]); ?>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'error-modal',
    'header' => '<h4 class="modal-title">' . Yii::t('app', 'Error') . '</h4>',
    'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal" id="error-close">Bezár</button>',

]); ?>
<?php Modal::end(); ?>

<?php Modal::begin([
	'id' => 'abandon-modal',
	'header' => '<h4 class="modal-title">Biztos?</h4>',
	'footer' => '<button type="button" class="btn btn-primary" data-dismiss="modal" id="abandon-confirm">Igen</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id="abandon-cancel">Nem</button>',

]); ?>
<?php Modal::end(); ?>

<?php
$this->registerJsFile("js/modal.view.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
?>