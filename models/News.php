<?php

namespace app\models;

use app\models\base\NewsBase;
use Exception;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;

class News extends NewsBase
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
            ],
        ];
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['file'], 'required'],
            [
                ['file'],
                'file',
                'skipOnEmpty' => true,
                'extensions' => ['png', 'jpg'],
                'maxSize' => 2 * 1024 * 1024, //2 MB
                'checkExtensionByMimeType' => true
            ]
        ]);
    }

    public function beforeValidate()
    {
        $this->file = UploadedFile::getInstance($this, 'file');

        if (!empty($this->file) && ($this->file instanceof UploadedFile)) {
            $this->file_name = $this->generateUniqueFileName($this->file->getExtension());
        }

        return parent::beforeValidate();
    }

    public function beforeSave($event)
    {
        if (parent::beforeSave($event)) {
            if (!empty($this->file)) {

                $path = Yii::$app->params['upload_dir'] . DIRECTORY_SEPARATOR;

                if (!$this->isNewRecord && is_file($path . $this->getOldAttribute('file_name'))) {
                    unlink($path . $this->getOldAttribute('file_name'));
                }

                if (!$this->isNewRecord && is_file($path . 'thumbnail' . DIRECTORY_SEPARATOR . $this->getOldAttribute('file_name'))) {
                    unlink($path . 'thumbnail' . DIRECTORY_SEPARATOR . $this->getOldAttribute('file_name'));
                }

                $this->file->saveAs($path . $this->file_name);

                Image::thumbnail($path . $this->file_name, 150, 150)
                    ->save($path . 'thumbnail' . DIRECTORY_SEPARATOR . $this->file_name);

            }
            return true;
        }
    }

    public function generateUniqueFileName($extension = 'jpg', $length = 24)
    {
        try {
            $randomString = Yii::$app->getSecurity()->generateRandomString($length) . '.' . $extension;

            if (!self::findOne(['file_name' => $randomString])) {
                return $randomString;
            } else {
                return $this->generateUniqueFileName($extension, $length);
            }

        } catch (Exception $e) {
            Yii::error($e->getMessage());
        }
    }
}
