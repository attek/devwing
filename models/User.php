<?php

namespace app\models;

use app\models\base\UserBase;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

class User extends UserBase implements IdentityInterface
{

    public $confirmPassword;

    private $passwordPattern = '/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/';

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => '\app\models\User',
                'message' => 'Ez az email cím már használatban van.',
            ],
            ['password', 'required', 'on' => 'default'],
            [
                'password',
                'match',
                'pattern' => $this->passwordPattern,
                'message' => 'Jelszónak 8 hosszúnak kelle lennie legalább egy számot egy kisbetüt egy nagybetüt kell tartalmaznia',
            ],
            ['confirmPassword', 'safe'],
            ['confirmPassword', 'string', 'min' => 8],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'confirmPassword' => 'Jelszó ismétlés',
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()
            ->validatePassword($password, $this->password);
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['username', 'name', 'email', 'password', 'confirmPassword', 'phone'];
        return $scenarios;
    }

}
