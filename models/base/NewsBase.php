<?php

namespace app\models\base;

use app\models\User;
use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id Id
 * @property string $title Cím
 * @property string $slug Slug
 * @property string $teaser Bevezető
 * @property string $body Tartalom
 * @property string|null $file_original_name Fájl eredeti neve
 * @property string|null $file_name Fájl
 * @property int|null $cr_user Létrehozó felhasználó
 * @property string|null $cr_date Létrehozás dátuma
 * @property int|null $mod_user Módosító felhasználó
 * @property string|null $mod_date Módosítás dátuma
 * @property int|null $status Státusz
 *
 * @property User $crUser
 * @property User $modUser
 */
class NewsBase extends ActiveRecordStatus
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'teaser', 'body'], 'required'],
            [['body'], 'string'],
            [['cr_user', 'mod_user', 'status'], 'integer'],
            [['cr_date', 'mod_date'], 'safe'],
            [['title', 'slug', 'teaser', 'file_original_name', 'file_name'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['cr_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['cr_user' => 'id']],
            [['mod_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['mod_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'title' => 'Cím',
            'slug' => 'Slug',
            'teaser' => 'Bevezető',
            'body' => 'Tartalom',
            'file_original_name' => 'Fájl eredeti neve',
            'file_name' => 'Fájl',
            'cr_user' => 'Létrehozó felhasználó',
            'cr_date' => 'Létrehozás dátuma',
            'mod_user' => 'Módosító felhasználó',
            'mod_date' => 'Módosítás dátuma',
            'status' => 'Státusz',
        ];
    }

    /**
     * Gets query for [[CrUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrUser()
    {
        return $this->hasOne(User::class, ['id' => 'cr_user']);
    }

    /**
     * Gets query for [[ModUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModUser()
    {
        return $this->hasOne(User::class, ['id' => 'mod_user']);
    }
}
