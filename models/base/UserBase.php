<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id Id
 * @property string $username Felhasználó név
 * @property string $name Teljes név
 * @property string|null $password Jelszó
 * @property string|null $auth_key Auth Key
 * @property string|null $password_hash Password hash
 * @property string|null $password_reset_token Password reset token
 * @property string $email Email
 * @property string|null $phone Telefonszám
 * @property int|null $cr_user Létrehozó felhasználó
 * @property string|null $cr_date Létrehozás dátuma
 * @property int|null $mod_user Módosító felhasználó
 * @property string|null $mod_date Módosítás dátuma
 * @property int|null $status Státusz
 *
 * @property UserBase $crUser
 * @property UserBase $modUser
 */
class UserBase extends ActiveRecordStatus
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'name', 'email'], 'required'],
            [['cr_user', 'mod_user', 'status'], 'integer'],
            [['cr_date', 'mod_date'], 'safe'],
            [['username', 'name', 'password', 'password_hash', 'password_reset_token', 'email', 'phone'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['auth_key'], 'unique'],
            [['cr_user'], 'exist', 'skipOnError' => true, 'targetClass' => UserBase::class, 'targetAttribute' => ['cr_user' => 'id']],
            [['mod_user'], 'exist', 'skipOnError' => true, 'targetClass' => UserBase::class, 'targetAttribute' => ['mod_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'username' => 'Felhasználó név',
            'name' => 'Teljes név',
            'password' => 'Jelszó',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password hash',
            'password_reset_token' => 'Password reset token',
            'email' => 'Email',
            'phone' => 'Telefonszám',
            'cr_user' => 'Létrehozó felhasználó',
            'cr_date' => 'Létrehozás dátuma',
            'mod_user' => 'Módosító felhasználó',
            'mod_date' => 'Módosítás dátuma',
            'status' => 'Státusz',
        ];
    }

    /**
     * Gets query for [[CrUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrUser()
    {
        return $this->hasOne(UserBase::class, ['id' => 'cr_user']);
    }

    /**
     * Gets query for [[ModUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModUser()
    {
        return $this->hasOne(UserBase::class, ['id' => 'mod_user']);
    }
}
