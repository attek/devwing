<?php

namespace app\models\base;

use app\components\ActiveRecordLogBehavior;
use \yii\db\ActiveRecord;
use Yii;
use yii\db\Expression;

abstract class ActiveRecordStatus extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DELETED = 3;

    public static function statusLabels()
    {
        return [
            self::STATUS_ACTIVE => 'Aktív',
            self::STATUS_INACTIVE => 'Inaktív',
            self::STATUS_DELETED => 'Törölt',
        ];
    }

    public function getStatus()
    {
        return $this->status === null ? null : self::statusLabels()[$this->status];
    }

    public function beforeSave($event)
    {
        if (parent::beforeSave($event)) {
            if ($this->isNewRecord) {
                if ($this->cr_date === null) {
                  $this->cr_date = new Expression('NOW()');
                }
                $this->cr_user = Yii::$app->user->id;
            } else {
                $this->mod_date = new Expression('NOW()');
                $this->mod_user = Yii::$app->user->id;
            }
            return true;
        }
    }

    public function getStatusForStyle()
    {
        if ($this->status === self::STATUS_INACTIVE) return "warning text-warning ";
        else if ($this->status === self::STATUS_DELETED) return "danger text-danger ";
        else return "";
    }

}