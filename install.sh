#!/bin/bash
docker-compose up -d

echo "Waiting for mysql container ..."
until docker logs devwing_db 2>&1 | grep -q "port: 3306  MySQL Community Server" 2>&1; do
  echo -n "."; sleep 0.25
done;
echo

docker exec devwing_php bash -c "
  composer install &&
  chmod 777 /app/runtime &&
  chmod 777 /app/web/assets &&
  cat /app/backup.sql | /usr/bin/mysql -uroot -pb5JXKmDY1vqx -hdb devwing
"
which xdg-open > /dev/null && xdg-open "http://localhost:8000" && exit 0
which open > /dev/null && open "http://localhost:8000" && exit 0