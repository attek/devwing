-- MySQL dump 10.18  Distrib 10.3.27-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: devwing
-- ------------------------------------------------------
-- Server version	10.3.27-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `title` varchar(255) NOT NULL COMMENT 'Cím',
  `slug` varchar(255) NOT NULL COMMENT 'Slug',
  `teaser` text NOT NULL COMMENT 'Bevezető',
  `body` text NOT NULL COMMENT 'Tartalom',
  `file_original_name` varchar(255) DEFAULT NULL COMMENT 'Fájl eredeti neve',
  `file_name` varchar(255) DEFAULT NULL COMMENT 'Fájl',
  `cr_user` int(11) DEFAULT NULL COMMENT 'Létrehozó felhasználó',
  `cr_date` timestamp NULL DEFAULT NULL COMMENT 'Létrehozás dátuma',
  `mod_user` int(11) DEFAULT NULL COMMENT 'Módosító felhasználó',
  `mod_date` timestamp NULL DEFAULT NULL COMMENT 'Módosítás dátuma',
  `status` int(4) DEFAULT NULL COMMENT 'Státusz',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `news_cr_user_fk` (`cr_user`),
  KEY `news_mod_user_fk` (`mod_user`),
  CONSTRAINT `news_cr_user_fk` FOREIGN KEY (`cr_user`) REFERENCES `user` (`id`),
  CONSTRAINT `news_mod_user_fk` FOREIGN KEY (`mod_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Nagyon keveset tudunk a koronavírus új változatáról, de könnyen vezethet egy nagyobb, rosszabb járványhoz','nagyon-keveset-tudunk-a-koronavirus-uj-valtozatarol-de-konnyen-vezethet-egy-nagyobb-rosszabb-jarvanyhoz','Egyelőre még nem sokat tudunk a koronavírus Dél-Kelet Angliában megjelent változatáról, de nagyon elővigyázatosnak kell lenni, hogy ne csússzunk bele egy ennél is nagyobb pandémiába.','<p>Egyelőre m&eacute;g nem sokat tudunk a koronav&iacute;rus D&eacute;l-Kelet Angli&aacute;ban megjelent v&aacute;ltozat&aacute;r&oacute;l, de nagyon elővigy&aacute;zatosnak kell lenni, hogy ne cs&uacute;sszunk bele egy enn&eacute;l is nagyobb pand&eacute;mi&aacute;ba.</p>\r\n\r\n<p>Amit eddig tudunk:</p>\r\n\r\n<p>Minden v&iacute;rus folyamatosan mut&aacute;l&oacute;dik, &iacute;gy az &ouml;nmag&aacute;ban nem meglepő, hogy a koronav&iacute;rusnak is megjelent egy mut&aacute;l&oacute;dott v&aacute;ltozata. Sőt az eddig ismert koronav&iacute;rus sem az a v&aacute;ltozat, ami Vuhanb&oacute;l indult, ugyanis Eur&oacute;p&aacute;ban a&nbsp;D614G mut&aacute;ci&oacute; v&aacute;lt glob&aacute;liss&aacute;, de mellette egy A222V n&eacute;vű mut&aacute;ci&oacute; is megjelent m&aacute;r kor&aacute;bban a kontinensen.&nbsp;</p>\r\n\r\n<p>Muge Cevik, a brit korm&aacute;ny &uacute;j l&eacute;g&uacute;ti v&iacute;rusokkal kapcsolatos tan&aacute;csad&oacute; csoportj&aacute;nak tagja szerint a koronav&iacute;rusnak eddig nagyj&aacute;b&oacute;l 4000 mut&aacute;ci&oacute;ja lehet, amiből k&ouml;r&uuml;lbel&uuml;l egy maroknyi az, amelyik jelenetős&eacute;ggel is b&iacute;r.A D&eacute;l-Kelet Angli&aacute;ban felfedezett vari&aacute;ns legfőbb tulajdons&aacute;ga, hogy 70 sz&aacute;zal&eacute;kkal fertőzőbb,&nbsp;&eacute;s 0,4 vagy ann&aacute;l nagyobb m&eacute;rt&eacute;kben n&ouml;velheti az R-&eacute;rt&eacute;ket, de nem hal&aacute;losabb, mint az eddig ismert fajt&aacute;ja (az R azt mutatja, hogy egy ember &aacute;tlagosan h&aacute;ny m&aacute;sikat fertőz meg, &eacute;s ha 1-n&eacute;l magasabb az &eacute;rt&eacute;ke, akkor a j&aacute;rv&aacute;ny erős&ouml;dik). A tud&oacute;sok viszont m&eacute;g egyelőre nem tesztelt&eacute;k laborat&oacute;riumi k&ouml;r&uuml;lm&eacute;nyek k&ouml;z&ouml;tt, hogy val&oacute;ban gyorsabban terjed-e ez a v&aacute;ltozat, egyelőre csak a tapsztalat mondatja ezt, &eacute;s azt sem tudj&aacute;k, hogy mi lehet az oka a gyorsabb terjed&eacute;snek. A legval&oacute;sz&iacute;nűbb forgat&oacute;k&ouml;nyv, hogy a mut&aacute;ci&oacute; azokban a legyeng&uuml;lt immunrendszerű szervezetekben alakult ki, amelyek nem tudt&aacute;k legyűrni a v&iacute;rust.&nbsp;</p>\r\n',NULL,'j1gOSNG8C3-q-YXFPnebl0uZ.jpg',1,'2020-12-21 10:15:45',NULL,NULL,1),(2,'Paul McCartney megmutatta, milyen lesz Peter Jackson Beatlesről szóló dokuja','paul-mccartney-megmutatta-milyen-lesz-peter-jackson-beatlesrol-szolo-dokuja','Ősszel került volna mozikba, de a járvány miatt húzodik a bemutatás, így csak 2021-ben lesz látható Peter Jackson dokumentumfilmje, amiben most nem hobbitok lesznek, hanem a Beatles tagjai. ','<p>Ősszel ker&uuml;lt volna mozikba, de a j&aacute;rv&aacute;ny miatt h&uacute;zodik a bemutat&aacute;s, &iacute;gy csak 2021-ben lesz l&aacute;that&oacute; Peter Jackson dokumentumfilmje, amiben most nem hobbitok lesznek, hanem a Beatles tagjai. A The Beatles: Get Back c&iacute;mű dokunak egyelőre nincs előzetese, de a rendező, hogy fokozza a hangulatot a rajong&oacute;kn&aacute;l, k&eacute;sz&iacute;tett egy &ouml;tperces vide&oacute;t, ezt osztotta meg Twitteren Paul McCartney.</p>\r\n\r\n<p>A vide&oacute;ban Jackson elmondja, hogy 56 &oacute;r&aacute;nyi felv&eacute;telből rakj&aacute;k &ouml;ssze a dokut, &eacute;s ez most nem egy előzetes, csak egy hangulatot mutat be, hogy tudjuk, mire sz&aacute;m&iacute;thatunk majd. (Egy viszonylag unalmas filmre, amiben a Beatles tagjai boh&oacute;ckodnak &eacute;s aminek minden m&aacute;sodik k&eacute;pkock&aacute;j&aacute;n feltűnik Yoko Ono.)</p>\r\n',NULL,'jRwAuU2q2W42pyzMgLwwc7ni.png',1,'2020-12-21 10:17:12',NULL,NULL,1),(3,'Két bolygó együttállása miatt karácsonyi csillag lesz az égen','ket-bolygo-egyuttallasa-miatt-karacsonyi-csillag-lesz-az-egen','A kijárási tilalom miatt most csak az ablakon keresztül lehet majd nézni az eget december 21-én éjjel, de állítólag érdemes lesz, mert most lehet majd legjobban látni a Jupiter és a Szaturnusz együttállását.','<p>A k&eacute;t bolyg&oacute; egy&uuml;tt&aacute;ll&aacute;sa miatt az&eacute;rt j&ouml;tt izgalom most mindenki, mert n&eacute;gy nap m&uacute;lva kar&aacute;csony, &eacute;s feltehetően a betlehemi csillag is valami hasonl&oacute; esem&eacute;nynek k&ouml;sz&ouml;nhetően volt l&aacute;that&oacute;, amikor a Biblia szerint megsz&uuml;letett J&eacute;zus. Ezt az elm&eacute;letet m&aacute;r Johannes Kepler is lejegyezte a 17. sz&aacute;zadban.</p>\r\n\r\n<p>Az &eacute;jjel d&eacute;lnyugat fel&eacute; kell majd n&eacute;zni, sz&oacute;val akinek ebbe az ir&aacute;nyba n&eacute;z az ablaka &eacute;s nincs előtte egy magas h&aacute;z, na meg nem lesznek felhők az &eacute;gen, annak szerencs&eacute;je lehet.&nbsp;</p>\r\n',NULL,'6D7rQGk7idyUoqzRdofBACWN.jpg',1,'2020-12-21 10:18:20',NULL,NULL,1),(4,'Negyedik napja 180 felett a járvány áldozatainak a száma','negyedik-napja-180-felett-a-jarvany-aldozatainak-a-szama','183 halálesetet jelentettek, megállt a kórházban lévők számának a csökkenése.','<p>183 &uacute;jabb &aacute;ldozata van a koronav&iacute;rusnak Magyarorsz&aacute;gon -&nbsp;<a href=\"https://koronavirus.gov.hu/cikkek/2-141-fovel-emelkedett-beazonositott-fertozottek-szama-es-elhunyt-183-beteg\" target=\"_blank\">der&uuml;lt ki</a>&nbsp;a koronavirus.gov.hu-n megjelent napi jelent&eacute;sből. M&aacute;r a negyedik napja 180 felett van a hal&aacute;lesetek sz&aacute;ma. Ilyen m&eacute;g nem volt kor&aacute;bban, december 3-5. k&ouml;z&ouml;tt volt a cs&uacute;cs, akkor viszont a negyedik napra m&aacute;r cs&ouml;kkent az &aacute;ldozatok sz&aacute;ma.&nbsp;</p>\r\n\r\n<p>A k&oacute;rh&aacute;zban l&eacute;vő fertőz&ouml;ttek sz&aacute;ma &ouml;tnapos cs&ouml;kken&eacute;s ut&aacute;n ism&eacute;t emelkedett 75-tel. 7097 koronav&iacute;rusos beteget &aacute;polnak k&oacute;rh&aacute;zakban.&nbsp;</p>\r\n',NULL,'qMwccA6__LLBEnkqsy61UhZ1.jpg',1,'2020-12-21 10:19:17',NULL,NULL,1),(5,'Maximum négyen tölthetik együtt az ünnepeket Szöulban','maximum-negyen-tolthetik-egyutt-az-unnepeket-szoulban','Példátlan szigorúság.','<p>Betiltj&aacute;k a n&eacute;gy r&eacute;sztvevősn&eacute;l nagyobb &ouml;sszej&ouml;veteleket Sz&ouml;ulban &eacute;s k&ouml;rny&eacute;k&eacute;n a kar&aacute;csonyi &eacute;s &uacute;j&eacute;vi &uuml;nnepek idej&eacute;re a koronav&iacute;rus-j&aacute;rv&aacute;ny megf&eacute;kez&eacute;se &eacute;rdek&eacute;ben - jelentett&eacute;k be h&eacute;tfőn a d&eacute;l-koreai főv&aacute;ros hat&oacute;s&aacute;gai.</p>\r\n\r\n<p>A d&eacute;l-koreai korm&aacute;ny vonakodik szigor&uacute; orsz&aacute;gos z&aacute;rlatot bevezetni, viszont a főv&aacute;ros, illetve az azt k&ouml;r&uuml;lvevő Kjonggi tartom&aacute;ny &eacute;s a Sz&ouml;ul melletti Incshon nagyv&aacute;ros vezet&eacute;se az orsz&aacute;g j&aacute;rv&aacute;nykezel&eacute;s&eacute;ben eddig p&eacute;lda n&eacute;lk&uuml;li korl&aacute;toz&aacute;sokat jelentett be a december 23. &eacute;s janu&aacute;r 3. k&ouml;z&ouml;tti időszakra.</p>\r\n\r\n<p>Csak &uacute;gy k&uuml;zdhetj&uuml;k le a v&aacute;ls&aacute;got, ha visszaszor&iacute;tjuk a csal&aacute;dtagokkal, bar&aacute;tokkal, koll&eacute;g&aacute;kkal val&oacute; tal&aacute;lkoz&aacute;sokat, mert ezek alkalm&aacute;val fokozott a csoportos fertőz&eacute;sek vesz&eacute;lye, mondta Szeo Dzsung Hjup sz&ouml;uli polg&aacute;rmester, hozz&aacute;t&eacute;ve, hogy ez az utols&oacute; es&eacute;ly, hogy megf&eacute;kezz&eacute;k a v&iacute;rus terjed&eacute;s&eacute;t.</p>\r\n\r\n<p>A n&eacute;gy főn&eacute;l magasabb l&eacute;tsz&aacute;m&uacute; &ouml;sszej&ouml;vetelek tilalma valamennyi belt&eacute;ri &eacute;s k&uuml;lt&eacute;ri esem&eacute;nyre vonatkozik az esk&uuml;vők &eacute;s a temet&eacute;sek kiv&eacute;tel&eacute;vel. Jelenleg az &ouml;sszej&ouml;vetelek l&eacute;tsz&aacute;m&aacute;nak felső korl&aacute;tja kilenc fő.</p>\r\n\r\n<p>Sz&ouml;ul &eacute;s a k&ouml;rnyező Kjonggi tartom&aacute;ny, valamint Incshon v&aacute;ros lakoss&aacute;ga &ouml;sszes&iacute;tve D&eacute;l-Korea 51 milli&oacute;s n&eacute;pess&eacute;g&eacute;nek nagyj&aacute;b&oacute;l fel&eacute;t teszi ki. D&eacute;l-Kore&aacute;ban a j&aacute;rv&aacute;ny kezdete &oacute;ta 50 591 esetet jegyeztek fel, a hal&aacute;lesetek sz&aacute;ma 698. A naponta diagnosztiz&aacute;lt &uacute;j fertőz&ouml;ttek sz&aacute;ma ezer k&ouml;r&uuml;l mozgott az ut&oacute;bbi napokban. (MTI)</p>\r\n',NULL,'p4hHi_4ctS4N4xWQmjMwAHCH.jpg',1,'2020-12-21 10:19:57',NULL,NULL,1);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `username` varchar(255) NOT NULL COMMENT 'Felhasználó név',
  `name` varchar(255) NOT NULL COMMENT 'Teljes név',
  `password` varchar(255) DEFAULT NULL COMMENT 'Jelszó',
  `auth_key` varchar(32) DEFAULT NULL COMMENT 'Auth Key',
  `password_hash` varchar(255) DEFAULT NULL COMMENT 'Password hash',
  `password_reset_token` varchar(255) DEFAULT NULL COMMENT 'Password reset token',
  `email` varchar(255) NOT NULL COMMENT 'Email',
  `phone` varchar(255) DEFAULT NULL COMMENT 'Telefonszám',
  `cr_user` int(11) DEFAULT NULL COMMENT 'Létrehozó felhasználó',
  `cr_date` timestamp NULL DEFAULT NULL COMMENT 'Létrehozás dátuma',
  `mod_user` int(11) DEFAULT NULL COMMENT 'Módosító felhasználó',
  `mod_date` timestamp NULL DEFAULT NULL COMMENT 'Módosítás dátuma',
  `status` int(4) DEFAULT NULL COMMENT 'Státusz',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `auth_key` (`auth_key`),
  KEY `user_cr_user_fk` (`cr_user`),
  KEY `user_mod_user_fk` (`mod_user`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','Admin','$2y$13$icc7qlMXVCgiCCox7Z8FsODVnCMeOJnrEKGltb4c/qeBECd8ucB1q','','','','admin@admin.com','',NULL,'2020-12-21 09:54:31',1,'2020-12-21 09:54:31',1),(2,'demo','Demo','$2y$13$cOwVcl/sCad5oEkuByxslu4AfBCO44N6hYqLyJN7aLnC/LiPd4QBy','iisxflTNSactDdssKe004vg8lguZ_aV_',NULL,NULL,'demo@demo.com','a',1,'2020-12-21 10:02:45',1,'2020-12-21 10:02:45',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-21 11:53:02
