jQuery(document).ready(function ($) {

    var item;
    var modalDelete = $('#delete-modal');
    var modalError = $('#error-modal');
    var containerId;

    $('body').on('click', '.activity-view-link', function (event) {
        event.preventDefault();
        var data = $(this).data();
        var url = data.url.split('/');
        url = '/' + url[1] + '/ajax-view';

        $.ajax({
            method: "GET",
            //type: "POST",
            url: url,
            data: {
                id: data.id
            },
            success: function (response) {

                var modal = $('#activity-modal');
                modal.find('.modal-title').html(data.title);
                modal.find('.modal-body').html(response);
                modal.modal();

            },
            error: function (xhr) {
                /*if (xhr.status == 400)
                    console.log(xhr.responseText, xhr.status);
                else
                    console.log(xhr.responseText, xhr.status);*/
                modalError.find('.modal-body').empty().html(xhr.responseText);
                modalError.modal();
            }
        });

    }).on('click', '.activity-delete-link', function (event) {
        event.preventDefault();
        item = $(this).data();
        containerId = '#' + $(this).closest('div').parent('div').attr('id');

        modalDelete.find('.modal-body').html(item.text);
        modalDelete.modal();

    });


    $('#delete-confirm').click(function (event) {
        event.preventDefault();
        $.ajax({
            method: "POST",
            url: item.url,
            data: {
                id: item.id
            },
            success: function (response) {
                $.pjax.reload({container: containerId});
            },
            error: function (xhr) {
                console.log(xhr.responseText, xhr.status);
                modalError.find('.modal-body').empty().html(xhr.responseText);
                modalError.modal();

            }
        });
    });

});